"""
A project routes that refers to note interaction
"""

from fastapi import APIRouter, Depends

from core.dependencies import contains_valid_token

from apps.notes.models import Note
from apps.notes.utils import create_note


router = APIRouter()


@router.post('/', status_code=201)
async def handle_note_creation(
    note: Note,
    token: str = Depends(contains_valid_token)
):
    """
    A route to create a new note by given data (body, notebook)

    Returns:
        Dict[str, Any]: Return data that contains created note title,
            content (rendered XML with given body) and notebook GUID.
    """

    note_data = await create_note(note.body, token, resources=note.resources)
    return note_data
