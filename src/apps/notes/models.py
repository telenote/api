from typing import Optional, List, Dict

from pydantic import BaseModel


class Note(BaseModel):
    """
    An Evernote API data that refers to note that has to be created.

    Attributes:
        body (str): Note body to be saved
        notebook (Optional[int]): A notebook GUID that note refers to.
    """

    body: str
    resources: Optional[List[Dict[str, str]]] = None
