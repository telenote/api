from evernote.edam.type.ttypes import Note

from utils import render_template_to_string


class NoteFactory:
    """
    A factory design to create Evernote's Note instances

    Attributes:
        TITLE_LENGTH (int): A words amount to be presented in note's title.
            Factory gets the note title as the first TITLE_LENGTH words
            of note's body.
        NOTE_TEMPLATE_NAME (str): A XML template name to be rendered by
            jinja2 (by default, templates are stored in templates/
            directory).
    """

    TITLE_LENGTH: int = 7
    NOTE_TEMPLATE_NAME: str = 'note.xml'

    @classmethod
    async def create(cls, body: str, resourses=None):
        """
        Creates a new Evernote note.

        Args:
            body (str): A note body to be created.
            notebook (Optional[int]): A Evernote Notebook GUID that refers
                to note that has to be created.

        Returns:
            Note: An evernote SDK Note instance
        """

        note = Note()

        note.title = await cls._get_note_title(body)
        note.content = await cls._render_note_content(body, resourses)

        return note

    @classmethod
    async def _get_note_title(cls, body: str):
        """
        Gets the note title as the first TITLE_LENGTH words of note's body

        Returns:
            str: A note title to be created.
        """

        words = body.split()

        if len(words) < cls.TITLE_LENGTH:
            return body

        first_n_words = words[:cls.TITLE_LENGTH]
        first_n_words.append('...')

        return ' '.join(first_n_words)

    @classmethod
    async def _render_note_content(cls, body, resourses=None):
        """
        Renders XML with given note's body.

        Returns:
            str: A rendered XML file
        """

        note_content =  await render_template_to_string(
            cls.NOTE_TEMPLATE_NAME,
            body=body,
            resourses=resourses
        )
        return note_content
