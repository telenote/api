import hashlib

from utils import get_evernote_client

from apps.notes.factories import NoteFactory

from apps.resources.formatters import ResourceFormatter


async def create_note(body, token, resources=None):
    """
    Creates a new user note.

    Args:
        body (str): A note's body to be created.
        token (str): A token of user that note should be created for.
        notebook (int): A notebook GUID that note should be placed in.

    Returns:
        Dict[str, str]: Dictionary, that retuls created note title and
            content.
    """
    formatter = ResourceFormatter(resources)
    formatted_resources = await formatter.format()

    note = await NoteFactory.create(body, formatted_resources)

    client = get_evernote_client(token=token)
    note_store = client.get_note_store()
    note_store.createNote(note)

    return {
        'title': note.title,
        'content': note.content
    }
