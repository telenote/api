from typing import Dict, Any

from core import settings
from core.database import db

from utils import get_evernote_client

from .exceptions import TokenError


class BaseTokenManager:
    """
    TokenManager is a class that manages the token data, i.e. it gets
    it from database and saves new value into it.
    """

    async def get(self):
        raise NotImplementedError()

    async def set(self, value):
        raise NotImplementedError()


class TemporaryTokenManager(BaseTokenManager):
    """
    Temporary token is a token recieved from Evernote OAuth API. It is used
    for getting authorization URL and permanent access token.
    """

    def __init__(self, chat_id: int):
        self.chat_id = chat_id
        self.client = get_evernote_client()

    async def get(self) -> Dict[str, str]:
        token_data = await self.get_token_dict()
        return token_data['oauth_token']

    async def get_token_dict(self):
        return self.client.get_request_token(settings.OAUTH_CALLBACK_URL)

    async def set(self, data: Dict[str, str]) -> Dict[str, str]:
        try:
            return await db.users.update_one(
                {
                    'chat_id': self.chat_id
                },
                {
                    '$set': {
                        'tokens.temporary': {
                            'token': data['oauth_token'],
                            'secret': data['oauth_token_secret']
                        }
                    }
                },
                upsert=True
            )
        except KeyError:
            raise TokenError(
                'Invalid data format, should be a dictionary: '
                '{"oauth_token": ..., "oauth_token_secret": ...}'
            )


class AccessTokenManager(BaseTokenManager):
    """
    Access token is Evernote OAuth token received after user has authorized
    application for his Evernote profile.
    """
    def __init__(self, temporary_token: str):
        self.temporary_token = temporary_token
        self.client = get_evernote_client()

    async def get(self, verifier: str):
        try:
            user_data = await self._get_user_data()
            return user_data['tokens']['access']
        except ValueError as error:
            raise TokenError(str(error))
        except KeyError:
            # Token does not exist yet, need to create it
            pass

        try:
            access_token = self.client.get_access_token(
                oauth_token=self.temporary_token,
                oauth_token_secret=user_data['tokens']['temporary']['secret'],
                oauth_verifier=verifier
            )
        except KeyError:
            raise TokenError('Invalid token/verifier data')

        return access_token

    async def set(self, access_token: str) -> Dict[str, Any]:
        return await db.users.update_one(
            {
                'tokens.temporary.token': self.temporary_token
            },
            {
                '$set': {
                    'tokens.access': access_token
                }
            }
        )

    async def _get_user_data(self):
        user_data = await db.users.find_one({
            'tokens.temporary.token': self.temporary_token
        })

        if not user_data:
            raise ValueError('Invalid temporary token')

        return user_data
