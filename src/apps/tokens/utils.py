from core.database import db

from utils import get_evernote_client

from .managers import TemporaryTokenManager


async def validate_user_token(token: str) -> bool:
    """
    Checks if the given token exists in database.

    Args:
        token (str): A users access token to be checked.

    Returns:
        bool: A logic statement that shows whether the given token
            was found in database or not.
    """

    return bool(await db.users.find_one({'tokens.access': token}))


async def get_authorize_url(chat_id: int) -> str:
    """
    Retrieved the autohirzation URL using the Evernote python SDK. It gets
    the temporary token during its work and saves it to database on the
    work finish.
    """
    manager = TemporaryTokenManager(chat_id)
    client = get_evernote_client()

    temporary_token_dict = await manager.get_token_dict()
    authorize_url = client.get_authorize_url(temporary_token_dict)

    if authorize_url:
        await manager.set(temporary_token_dict)

    return authorize_url
