from fastapi import APIRouter, HTTPException

from .exceptions import TokenError
from .managers import AccessTokenManager
from .models import Token, AuthorizationResult
from .utils import validate_user_token


router = APIRouter()


@router.post('/access/', name='access_token')
async def get_access_token(oauth: AuthorizationResult):
    """
    Gets the temporary token and user verifier value, sends request for
    getting the Evernote access token for user. Returns the access token
    value on success.

    Args:
        oauth (AuthorizationResult): A parameters received on callback_url
            defined on previous OAuth stage (receiving the temporary token).
            Contains the temporary token value and user verifier value (if
            provided - then the user has authorized application for his
            Evernote Profile). Details in AuthorizationResult docs.

    Raises:
        HTTPException: On the token retrieve failure

    Returns:
        Dict[str, str]: {'access_token': <token value>, 'success': true/false}
    """

    manager = AccessTokenManager(oauth.token)

    try:
        access_token = await manager.get(oauth.verifier)
        await manager.set(access_token)
    except TokenError as error:
        raise HTTPException(status_code=400, detail=str(error))

    return {'access_token': access_token}


@router.post('/validate/', name='validate_token')
async def validate_token(token: Token):
    """
    Validates the user token (checks if the token presented in users
    database collection)

    Returns:
        Dict[str, bool]: A dictionatry -- {'is_valid': validation result}
    """

    is_valid = await validate_user_token(token.access_token)
    return {'is_valid': is_valid}
