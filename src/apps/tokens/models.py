from pydantic import BaseModel


class Token(BaseModel):
    """
    An Evernote API access token data that user sends to make it stored.
    Details: https://dev.evernote.com/doc/articles/authentication.php#callback

    Attributes:
        access_token (str): Access token got afther succcessfull Evernote
            OAuth login.
    """

    access_token: str


class AuthorizationResult(BaseModel):
    """
    An Evernote API OAuth authorization result. Parameters given to defined
    callback url.

    Attributes:
        token (str): Temporary OAuth token value
        verifier (str): User verifier value. If it was provided then user
            has granted permission to this application to manage his data.
    """

    token: str
    verifier: str = None
