class TokenError(Exception):
    """
    This exceptions raises when TokenManager work resulted into fail
    """
    pass
