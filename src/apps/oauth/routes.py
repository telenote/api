"""
An API routes that refers to Evernote OAuth
"""

from fastapi import APIRouter

from apps.tokens.utils import get_authorize_url

from .models import UserChat


router = APIRouter()


@router.get('/authorize-url/', name='authroize_url')
async def obtain_authorize_url(user_chat: UserChat):
    """
    Returns the URL to authorize the application with user's personal
    evernote account.

    Args:
        user_char (UserChat): A user with bot chat representation

    Returns:
        Dict[str, str]: A result dictonary with authorization URL
    """

    url = await get_authorize_url(user_chat.chat_id)
    return {'authorize_url': url}
