from apps.resources.hashers import FileHasher


class ResourceFormatter:
    def __init__(self, payload):
        self._payload = payload

    async def format(self):
        if not self._payload:
            return

        if isinstnace(self._payload, list):
            data = self._format_many()
        else:
            data = self._format_one(self._payload)

        return data

    async def _format_many(self):
        formated_resources = []

        for resource in self._payload:
            formatted_data = await self._format_one(resource)

        return formated_resources

    async def _format_one(self, resourse):
        file_bytes = await get_resourse_bytes(resource['url'])
        hasher = FileHasher(file_bytes)

        return {
            'type': resourse['type'],
            'hash': hasher.get_hash()
        }
