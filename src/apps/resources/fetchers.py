import aiohttp


async def fetch_resource(resource_url):
    async with aiohttp.ClientSession() as session:
        async with session.get(resource_url) as response:
            if response.status == 200:
                return await response.read()
