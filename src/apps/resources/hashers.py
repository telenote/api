import hashlib


class FileHasher:
    def __init__(self, file_bytes):
        self.file_bytes = file_bytes

    def get_hash(self):
        _hash = hashlib.md5(self.file_bytes)
        return _hash.hexdigest()
