from evernote.api.client import EvernoteClient

from core import settings


def get_evernote_client(
    consumer: str = settings.EVERNOTE_CONSUMER_KEY,
    secret: str = settings.EVERNOTE_CONSUMER_SECRET,
    sandbox: bool = settings.EVERNOTE_USE_SANDBOX,
    token: str = None
):
    """
    A factory function that creates EvernoteClient instance by given
    parameters. Defaults to project configuration.

    Args:
        consumer (Optional[str]): A registred in Evernote API
            application consumer key value. Defaults to
            `settigs.EVERNOTE_CONSUMER_KEY`.
        secret (Optional[str]): A registred in Evrenote API application
            consumer secret value. Defaults to
            `settins.EVERNOTE_CONSUMER_SECRET`.
        sandbox (Optional[bool]): A logic statement that shows whether
            evernote client should use Evernote Sandbox (dev application)
            or not. Defaults to `settings.EVERNOTE_USE_SANDBOX`
        token (Optional[str]): A token of user whos data should be
            accessed via client. Defaults to None.

    Returns:
        EvernoteClient: Created EvernoteCLient instance
    """

    return EvernoteClient(
        consumer_key=consumer,
        consumer_secret=secret,
        sandbox=sandbox,
        token=token
    )


async def render_template_to_string(
    template_name: str,
    **template_kwargs
) -> str:
    """
    Renders template by given name and context parameters

    Args:
        template_name (str): A name of template that should be rendered.
            A file name from projects templates/ folder.
        **template_kwargs (dict): A template context.

    Returns:
        str: Rendered template as string
    """

    template = settings.JINJA2_ENV.get_template(template_name)
    return await template.render_async(**template_kwargs)
