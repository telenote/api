import pytest

from apps.notes.utils import create_note



@pytest.mark.asyncio
async def test_create_note(note_body, valid_token):
    note_data = await create_note(note_body, valid_token)

    assert 'title' in note_data
    assert 'content' in note_data
