import pytest

from apps.tokens.managers import TemporaryTokenManager, AccessTokenManager


class TestTemporaryTokenManager:
    pytestmark = pytest.mark.asyncio

    @pytest.fixture(scope='class')
    def chat_id(self) -> int:
        return 123

    @pytest.fixture(scope='class')
    async def token_data(self, chat_id) -> dict:
        manager = TemporaryTokenManager(chat_id)
        token_dict = await manager.get_token_dict()
        return token_dict

    async def test_temporary_token_manager_get_token_dict(self, token_data):
        assert 'oauth_token' in token_data
        assert 'oauth_token_secret' in token_data

    async def test_temporary_token_manager_set(self, token_data, chat_id):
        manager = TemporaryTokenManager(chat_id)

        data = await manager.set(token_data)

        assert data is not None
