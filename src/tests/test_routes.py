from __future__ import absolute_import

from fastapi.testclient import TestClient

from main import app


client = TestClient(app)


def test_obtain_authorize_url():
    response = client.get('/api/oauth/authorize-url/', json={
        'chat_id': 123
    })

    data = response.json()

    assert response.status_code == 200
    assert 'authorize_url' in data


def test_get_accesS_token_invalid():
    response = client.post('/api/tokens/access/', json={
        'token': '12312377asjduas732ujajsd',
        'verifier': '12381238uasdasjnvshfdjasdnasdhbhacn7asd'
    })

    data = response.json()

    assert response.status_code == 400
    assert 'detail' in data


class TestValidateTokenView:
    def test_validate_token(self, valid_token):
        response = client.post('/api/tokens/validate/', json={
            'access_token': valid_token
        })

        data = response.json()

        assert response.status_code == 200
        assert data['is_valid']

    def test_validate_token_invalid_token(self):
        response = client.post('/api/tokens/validate/', json={
            'access_token': '123'
        })

        data = response.json()

        assert response.status_code == 200
        assert not data['is_valid']


class TestCreateNoteView:
    def test_create_note(self, valid_token):
        response = client.post(
            '/api/notes/',
            json={
                'body': (
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
                    'Vestibulum id sollicitudin velit. Praesent convallis enim.'
                )
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Token {valid_token}'
            }
        )

        data = response.json()

        assert response.status_code == 201
        assert 'title' in data
        assert 'content' in data

    def test_create_note_invalid_token(self):
        response = client.post(
            '/api/notes/',
            json={
                'body': 'test'
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Token 123'
            }
        )

        assert response.status_code == 401
