import asyncio

import pytest

from apps.notes.factories import NoteFactory



class TestNoteFactory:
    pytestmark = pytest.mark.asyncio

    async def test_note_factory_create(self, note_body, event_loop):
        expected_title = (
            'Lorem ipsum dolor sit amet, consectetur adipiscing ...'
        )

        note = await NoteFactory.create(body=note_body)

        assert note.title == expected_title
        assert '<en-note>' in note.content
        assert note_body in note.content

    async def test_note_factory_create_with_resourses(
        self,
        event_loop,
        note_body
    ):
        resourses = [
            {
                'type': 'image/jpeg',
                'hash': '123456789'
            }
        ]

        note = await NoteFactory.create(
            body=note_body,
            resourses=resourses
        )

        rendered_resourse = '<en-media type="image/jpeg" hash="123456789"/>'
        assert rendered_resourse  in note.content
