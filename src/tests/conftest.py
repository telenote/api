from __future__ import absolute_import

import asyncio

import pytest

from core import settings
from core.database import client, db


loop = asyncio.get_event_loop()


def pytest_sessionstart(session):
    pass


def pytest_sessionfinish(session, exitstatus):
    loop.run_until_complete(client.drop_database('test'))


@pytest.fixture(scope='session')
def event_loop():
    return loop


@pytest.fixture(scope='session')
async def valid_token(event_loop):
    token_value = settings.EVERNOTE_TOKEN

    await db.users.insert_one({
        'tokens': {
            'access': token_value
        }
    })

    return token_value

@pytest.fixture(scope='session')
def note_body():
    return (
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
        'Vestibulum id sollicitudin velit. Praesent convallis enim.'
    )
