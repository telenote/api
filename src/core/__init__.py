"""
The core module contains the API that being used a project-wide, e.g.
the project configuration/settings, database connections, exceptions
definitions and so on.
"""
