"""
Database client definition. Import the `db` variable from this module
in order to access the database features.
"""

import asyncio

from motor.motor_asyncio import AsyncIOMotorClient

from core import settings

loop = asyncio.get_event_loop()
client = AsyncIOMotorClient(
    f'mongodb://{settings.MONGO_USERNAME}:{settings.MONGO_PASSWORD}'
    f'@{settings.MONGO_HOST}',
    io_loop=loop
)

db = client[settings.DB_NAME]
