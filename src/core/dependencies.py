"""
FastAPI `dependencies` module. The dependencies are defined as:

"Dependency Injection" means, in programming, that there is a way
for your code (in this case, your path operation functions) to declare
things that it requires to work and use: "dependencies".

And then, that system (in this case FastAPI) will take care of doing
whatever is needed to provide your code with those needed dependencies
("inject" the dependencies).

This is very useful when you need to:
    - Have shared logic (the same code logic again and again).
    - Share database connections.
    - Enforce security, authentication, role requirements, etc.
    - And many other things...
"""
import re

from fastapi import Header, Depends, HTTPException

from apps.tokens.utils import validate_user_token


async def contains_authorization_header(authorization: str = Header(None)):
    """
    A dependency that checks whether Authorization: Token HTTP-header was
    provided within the request.

    Args:
        authorization (str): Authorization HTTP-header value (i.e.
            `Token ASDFGH12345`)

    Returns:
        str: Parsed token from HTTP header (i.e. `ASDFGH12345`)
    """

    authorization_header_regex = re.compile(r'^Token\s\S+$')

    if not authorization_header_regex.match(authorization):
        raise HTTPException(
            status_code=401,
            detail='No Authorization header was provided or provided '
            'Authorization header is invalid. Should be: Authorization Token '
        )

    token = authorization.split()[1]
    return token


async def contains_valid_token(
    token: str = Depends(contains_authorization_header)
):
    """
    Checks whether the provided in Authorization HTTP-header token
    is valid (presented in users database collection).

    Args:
        token (str): A token to be validated

    Returns:
        str: A validated token
    """

    is_valid = await validate_user_token(token)

    if not is_valid:
        raise HTTPException(
            status_code=401,
            detail=f'A token {token} does not exist'
        )

    return token
