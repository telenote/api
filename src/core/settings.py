"""
An API configuration file
"""

from starlette.config import Config

from jinja2 import Environment, select_autoescape, FileSystemLoader

config = Config('../../.deploy/.env')

# Genral app config
DEBUG = config('DEBUG', cast=bool, default=True)
DB_NAME = config('DB_NAME', cast=str, default='telenote')
OAUTH_CALLBACK_URL = config(
    'OAUTH_CALLBACK_URL',
    cast=str,
    default='http://127.0.0.1:5000/complete/'
)

# Evernote config
EVERNOTE_CONSUMER_KEY = config('EVERNOTE_CONSUMER_KEY', cast=str)
EVERNOTE_CONSUMER_SECRET = config('EVERNOTE_CONSUMER_SECRET', cast=str)
EVERNOTE_USE_SANDBOX = config('EVERNOTE_USE_SANDBOX', cast=bool, default=True)
EVERNOTE_TOKEN = config('EVERNOTE_TOKEN', cast=str)

# Useful const urls
TELEGRAM_BOT_URL = 'https://t.me/evernote_noting_bot'

# Database config
MONGO_USERNAME = config('MONGO_USERNAME', cast=str)
MONGO_PASSWORD = config('MONGO_PASSWORD', cast=str)
MONGO_HOST = config('MONGO_HOST', cast=str)

# Jinja2 config
JINJA2_ENV = Environment(
    loader=FileSystemLoader('templates/'),
    autoescape=select_autoescape('html', 'htm', 'xml'),
    enable_async=True
)
