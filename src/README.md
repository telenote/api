# API specification

Telenote provides a REST API for handling the Evernote authentication and note creation. API is written in use with the following third-party libraries, packages and frameworks:

- [FastAPI](https://github.com/tiangolo/fastapi "FastAPI Github repository");
- [Evernote SDK for Python 3](https://github.com/evernote/evernote-sdk-pytho3 "Evernote SDK for Python 3 Github repository");
- [Motor](https://github.com/mongodb/motor "Motor Github repository")
- [poetry](https://github.com/python-poetry/poetry "Poetry Github repository")


## Deploying with Poetry

To deploy a project using the poetry tool do the following:

1. Install all the API dependencies: `poetry install`
2. Set the `EVERNOTE_CONSUMER_KEY`, `EVERNOTE_CONSUMER_SECRET`, `EVERNOTE_USE_SANDBOX`, `MONGO_USERNAME`, `MONGO_PASSWORD`, `MONGO_HOST`, `DEBUG` environment variables.
3. Run the mongo database service.
4. Execute `poetry run uvicorn main:app` to run the uvicorn server.

## Testing

To run tests do the following:

1. Install all the API dependencies: `poetry install`
2. Set the `EVERNOTE_TOKEN` environemnt variable.
3. Execute `poetry run python3 -m pytest -x`.
